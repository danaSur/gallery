#include "DatabaseAccess.h"

//declaring again static variables:
std::list<Album> DatabaseAccess::albums;
std::list<Picture> DatabaseAccess::pictures;
std::list<User> DatabaseAccess::users;

bool DatabaseAccess::open()
{
	
	int res;
	res = sqlite3_open(db_name.c_str(), &db);
	int doesFileExist = _access(db_name.c_str(), 0);
	if (res)
	{
		//there's an error while trying to open the DB, and it failed
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return false;
	}
	else
	{
		//the DB opened.
		//	fprintf(stdout, "Opened database successfully\n");	
	}
	if (doesFileExist != 0)
	{
		// init database:
		//sending a querys to the SQL server, to create the 4 tables. 
		//if there is an error during the proccess, an exception will be thrown.
		const char* UsersTable = "CREATE TABLE Users(" \
								 "ID INTEGER PRIMARY KEY, "\
								 "Name text NOT NULL"\
			                     ");";

		const char* AlbumsTable = "CREATE TABLE Albums("\
								  "ID INTEGER PRIMARY KEY,"\
								  "Name text NOT NULL,"\
								  "Creation_Date text NOT NULL,"\
								  "User_ID integer not null,"\
								  "FOREIGN KEY(User_ID) REFERENCES Users(ID) "\
						       	  ");";

		const char* PicturesTable = "CREATE TABLE Pictures("\
									"ID integer PRIMARY KEY,"\
									"Name text NOT NULL,"\
									"Location text NOT NULL,"\
									"Creation_Date text NOT NULL,"\
									"Album_ID integer NOT NULL,"\
									"FOREIGN KEY(Album_ID) REFERENCES Albums(ID)"\
									");";
		const char* TagsTable = "CREATE TABLE Tags("\
								"ID INTEGER PRIMARY KEY,"\
								"Picture_ID integer NOT NULL,"\
								"User_ID integer NOT NULL,"\
								"FOREIGN KEY(Picture_ID) REFERENCES Pictures(ID),"\
								"FOREIGN KEY(User_ID) REFERENCES Users(ID) "\
								");";

		if (sendToSQL(UsersTable) == false)
			throw MyException("something went wrong with creating table Users\n");
		if (sendToSQL(AlbumsTable) == false) 
			throw MyException("something went wrong with creating table Albums\n");
		if (sendToSQL(PicturesTable) == false) 
			throw MyException("something went wrong with creating table Pictures\n");
		if (sendToSQL(TagsTable) == false) 
			throw MyException("something went wrong with creating table Tags\n");
	}
}

void DatabaseAccess::clear()
{
	//sending a querys to the SQL server, to delete everything from every table. 
	//if there is an error during the proccess, an exception will be thrown.
	const char* UsersTable = "DELETE FROM Users;";
	const char* AlbumsTable = "DELETE FROM Albums;";
	const char* PicturesTable = "DELETE FROM Pictures;";
	const char* TagsTable = "DELETE FROM Tags;";

	//sending each one to an private function of class DatabaseAccess, that return true if the query succeeded and false if not
	if (sendToSQL(UsersTable) == false)
		throw MyException( "something went wrong with deleting table Users\n" );
	if (sendToSQL(AlbumsTable) == false)
		throw MyException("something went wrong with deleting table Albums\n");
	if (sendToSQL(PicturesTable) == false)
		throw MyException("something went wrong with deleting table Pictures\n");
	if (sendToSQL(TagsTable) == false)
		throw MyException("something went wrong with deleting table Tags\n");
}

void DatabaseAccess::cleanUserData(const User& userId)
/*
	the function deletes all of the albums and pictures of a user. 
*/
{
	std::list<Album> albumsOfUser = getAlbumsOfUser(userId);
	std::list<Album>::iterator it;

	//deleting every picture from every album of the user
	for (it = albumsOfUser.begin(); it != albumsOfUser.end(); ++it)
	{
		const char* GetUsers = "delete from pictures where album_id =";
		std::string result = GetUsers + std::to_string(getId(it->getName())) + ";";
		if (sendToSQL(result.c_str()) == false)
			throw MyException("something went wrong with deleting the pictures from the albums of user " + userId.getName() + "\n");
	}

	//deleting all the albums of the user
	const char* GetUsers = "delete from albums where user_id =";
	std::string result = GetUsers + std::to_string(userId.getId()) + ";";
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with deleting the albums of user " + userId.getName() + "\n");
}

// auto DatabaseAccess::getAlbumIfExists(const std::string& albumName)
//{
//	const char* GetAlbum = "SELECT * FROM ALBUMS WHERE NAME ='";
//	std::string result = GetAlbum + albumName + "';";
//
//	Album* palbum = new Album();
//
//	char* zErrMsg = 0;
//	/* Execute SQL statement
//	   we are sending an empty User to the function, and we get it back from 'getAlbumsFromServer' with all the new data
//	*/
//	int res = sqlite3_exec(db, result.c_str(), getAlbumsFromServer, palbum, &zErrMsg);
//
//	if (res != SQLITE_OK)
//	{
//		std::string err = zErrMsg;
//		sqlite3_free(zErrMsg);
//		throw MyException("SQL error: " + err + "\n");
//	}
//
//	if (palbum->getName() == albumName)
//		return true;
//	else
//		return false;
//	
//}


// queries:

 User DatabaseAccess::getTopTaggedUser()
/*
	the function finds the user that has the most taggs in all the photos in the server,
	and return the User (object) of the user that has the most tags.
*/
{
	//getting a list of all users and it's iterator, and setting 2 variables to help with compering.
	std::list<User> allUsers = getAllUsers();
	std::list<User>::iterator it;
	int maxTag = 0;
	User maxTaggedUser;

	//in the loop, i get every user's number of taggs, with the function 'countTagsOfUser', that receives the user as a parameter.
	for (it = allUsers.begin(); it != allUsers.end(); ++it)
	{
		int itTags = countTagsOfUser(*it);
		//whenever the 'itTags' has bigger value then 'maxTag', 
		//'maxTag' gets the value of 'itTags' and 'maxTaggedUser' gets the 'it' values as well.  
		if (itTags > maxTag)
		{
			maxTag = itTags;
			maxTaggedUser = *it;
		}
	}
	return maxTaggedUser;
		
}

Picture DatabaseAccess::getTopTaggedPicture()
/*
	the function finds the picture that has the most tags in it,
	and return the Picture (object) of the pic that has the most tags.
*/
{
	//it's the same principle like the function above,'getTopTaggedPicture', compering between each picture and returning the one with the most tags.
	std::list<Picture> allPics = getAllPictures();
	int maxTag = 0;
	Picture maxTaggedPic;
	std::list<Picture>::iterator it;
	for (it = allPics.begin(); it != allPics.end(); ++it)
	{
		int itTags = getTagsCount(it->getId());
		if (itTags > maxTag)
		{
			maxTag = itTags;
			maxTaggedPic = *it;
		}
	}
	return maxTaggedPic;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
/*
	function finds the list of all the pictures that the user is tagged in them.
*/
{
	//getting the list of all the photos from the server, iterator for this list, and an empty list for the tagged pictures.
	std::list<Picture> AllPics = getAllPictures();
	std::list<Picture> TaggedPics;
	std::list<Picture>::iterator it;

	for (it = AllPics.begin(); it != AllPics.end(); ++it)
	{
		//if the user is tagged in this picture, then it will be pushed back into the list of the tagged picture.
		if (isUserTagged(it->getId(), user.getId()))
			TaggedPics.push_back(*it);
	}
	return TaggedPics;

}




//Album Related:

const std::list<Album> DatabaseAccess::getAlbums()
/*
	the function return a list of all the albums in the server
*/
{
	//the query to send
	std::string result = "SELECT * FROM ALBUMS ;"; 

	//an album object to send, it will get the values of each album in the server and will be pushed back into the static list of albums - 'albums'.
	Album* palbum = new Album();

	char* zErrMsg = 0;

	//Execute SQL statement
    //we are sending an Album User to the function, and we get it back from 'getAllAlbumsFromServer' with all the new data
	int res = sqlite3_exec(db, result.c_str(), getAllAlbumsFromServer, palbum, &zErrMsg);

	//if the query failed:
	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}

	//since all the albums are in the list 'albums', a new list gets all the data inside it and the static list is cleared from old data.
	std::list<Album> allAlbums = this->albums; 
	this->albums.clear();
	return allAlbums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
/*
	the function finds all the albums of a specific user, 
	and returns them as a list.
*/
{
	//query will look like this: "SELECT * FROM ALBUMS WHERE USER_ID = 12;"
	const char* GetUsers = "SELECT * FROM ALBUMS WHERE USER_ID =";
	std::string result = GetUsers + std::to_string(user.getId()) + ";";
	Album* palbum = new Album();

	char* zErrMsg = 0;
	/*
		Execute SQL statement
		we are sending an empty Album to the function, and we get it back from 'getAllAlbumsFromServer' with all the new data
	*/
	int res = sqlite3_exec(db, result.c_str(), getAllAlbumsFromServer, palbum, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}

	//since all the albums are in the list 'albums', a new list gets all the data inside it and the static list is cleared from old data.
	std::list<Album> allAlbums = this->albums;
	this->albums.clear();
	return allAlbums;
	
}

void DatabaseAccess::createAlbum(const Album& album)
/* 
	the function sends a query to the server with the new values to insert into the table 'albums'.
*/
{
	//query will look like this: "insert into albums(name, user_id, creation_date) values ('Trip', 2, "12/3/2020");"
	const char* GetUsers = "insert into albums(name, user_id, creation_date) values ('";
	std::string result = GetUsers + album.getName() + "', " + std::to_string(album.getOwnerId()) + ", '" + album.getCreationDate() + "') ;";
	if (sendToSQL(result.c_str()) == false)
		std::cout << "something went wrong..." << std::endl;
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
/*
	the function deletes from the server an album. 
*/
{
	//query will look like this: "DELETE FROM ALBUMS WHERE NAME ='trip';"
	const char* GetUsers = "DELETE FROM ALBUMS WHERE NAME ='";
	std::string result = GetUsers + albumName + "';";
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with deleting the album " + albumName + "\n");
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
/*
	the function checks if there is an album with that name in the server.
	if there is an object like that, the function will compare it with the name and the owner id, just to make sure it is the same.
*/
{
	//query will look like this: "SELECT * FROM ALBUMS WHERE NAME ='trip';"
	const char* GetAlbum = "SELECT * FROM ALBUMS WHERE NAME ='";
	std::string result = GetAlbum + albumName + "';";

	Album* palbum = new Album();

	char* zErrMsg = 0;
	/*
		Execute SQL statement
		we are sending an empty User to the function, and we get it back from 'getAlbumsFromServer' with all the new data
	*/
	int res = sqlite3_exec(db, result.c_str(), getAlbumsFromServer, palbum, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw MyException("SQL error: " + err + "\n");
	}
	if (palbum->getOwnerId() == userId && palbum->getName() == albumName)
		return true;
	else
		return false;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
/*
	the function will return an object to an album, according to it's name.
*/
{
	const char* GetAlbum = "SELECT * FROM ALBUMS WHERE NAME ='";
	std::string result = GetAlbum + albumName + "';";

	Album* palbum = new Album();

	char* zErrMsg = 0;
	//Execute SQL statement
	// we are sending an empty Album to the function, and we get it back from 'getAlbumsFromServer' with all the new data
	int res = sqlite3_exec(db, result.c_str(), getAlbumsFromServer, palbum, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw MyException("SQL error: " + err + "\n");
	}

	return *palbum;
}

void DatabaseAccess::printAlbums()
/*
	function will get all the albums from the server and prints them.
	the printing will be from the function 'callback', that the function 'sendToSQL' calls to, which this function calls to.
*/
{
	const char* GetAlbums = "SELECT * FROM Albums;";
	if(sendToSQL(GetAlbums) == false ) 
		throw MyException("something went wrong with printing the albums.\n");

}

int DatabaseAccess::getId(const std::string& albumName)
/*
	function return the id of an album, according to it's name.
*/
{
	//query will look like this: "SELECT id FROM ALBUMS WHERE NAME ='trip';"
	const char* Getid = "select id from albums where name = '";
	std::string result = Getid + albumName + "';";

	char* zErrMsg = 0;
	int id = 0;
	//now the function that will be operated because of the call to 'sqlite3_exec' is the function 'getnum', that gets as another argumant a number, int.
	// the result will be inserted into 'id'.
	int res = sqlite3_exec(db, result.c_str(), getnum, &id, &zErrMsg);
	if (res != SQLITE_OK)
	{
		std::string err;
		if (zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}
	return id;
}



//Picture Related:

bool DatabaseAccess::doesPictureExists(const std::string& name)
/*
	the function checks if there is an album with that name in the server.
	if there is an object like that, the function will compare it with the name, just to make sure it is the same.
*/
{
	//query will look like this: "SELECT * FROM Pictures WHERE NAME ='trip';"
	const char* GetPicture = "SELECT * FROM Pictures WHERE NAME ='";
	std::string result = GetPicture + name + "';";

	Picture* pPic = new Picture();

	char* zErrMsg = 0;

	//Execute SQL statement
	//we are sending an empty Picture to the function, and we get it back from 'getPictureFromServer' with all the new data.

	int res = sqlite3_exec(db, result.c_str(), getPictureFromServer, pPic, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw MyException("SQL error: " + err + "\n");
	}
	if (pPic->getName() == name)
		return true;
	else
		return false;
}

std::list<Picture> DatabaseAccess::getPictures(const std::string& albumName)
/*
	the function return al list of all the pictures that belong to the same album.
*/
{
	//query will look like this: "SELECT * FROM pictures WHERE ALBUM_ID = 3;"
	const char* GetUsers = "SELECT * FROM pictures WHERE ALBUM_ID =";
	std::string result = GetUsers + std::to_string(getId(albumName)) + ";";

	Picture* pPic = new Picture();

	char* zErrMsg = 0;

	//Execute SQL statement, 
	//we are sending a Picture to the function, and we get it back from 'getAllPicturesFromServer' with all the new data.
	
	int res = sqlite3_exec(db, result.c_str(), getAllPicturesFromServer, pPic, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}

	//since all the pictures that answer this filter are in the static list 'pictures', 
	// the variable 'allPics' gets the data from it, and 'pictures' is being cleared for it's next use.
	std::list<Picture> allPics = this->pictures;
	this->pictures.clear();
	return allPics;
}

std::list<Picture> DatabaseAccess::getAllPictures()
/*
	funtion return a list of all the pictures in the server.
*/
{
	std::string result = "select * from pictures;";

	Picture* pPic = new Picture();

	char* zErrMsg = 0;

	//Execute SQL statement
	//we are sending a Picture object to the function, and we get it back from 'getAllPicturesFromServer' with all the new data
	int res = sqlite3_exec(db, result.c_str(), getAllPicturesFromServer, pPic, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}

	//since all the pictures that answer this filter are in the static list 'pictures', 
	// the variable 'allPics' gets the data from it, and 'pictures' is being cleared for it's next use.
	std::list<Picture> allPics = this->pictures;
	this->pictures.clear();
	return allPics;
}

Picture DatabaseAccess::getPicture(const std::string& pictureName)
{
	//query will look like this: "SELECT * FROM Pictures WHERE NAME ='trip';"
	const char* GetUsers = "SELECT * FROM pictures WHERE name ='";
	std::string result = GetUsers + pictureName + "';";

	Picture* pPic = new Picture();

	char* zErrMsg = 0;
	//Execute SQL statement, 
	//we are sending a Picture to the function, and we get it back from 'getAllPicturesFromServer' with all the new data, after a casting.
	int res = sqlite3_exec(db, result.c_str(), getPictureFromServer, pPic, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}
	return *pPic;
}

int DatabaseAccess::getTagsCount(const int& pictureId)
/*
	the function return the number of tags that are in a picture, according to it's Id.
	the query function 'count(*)' return a number that, in this query, represent the number of timess the picture id shows in the tags table,
	and there for it represents the number of tags that are in the picture.
*/
{
	//query will look like this: " select count(*) from tags where picture_id = 4; "
	const char* GetTags = "select count(*) from tags where picture_id = ";
	std::string result = GetTags + std::to_string(pictureId) + ";";

	char* zErrMsg = 0;
	int count = 0;

	// like in the function 'getId' (from the album functions), the call to the callback function will be to the function 'getnum' 
	//and the number will enter into 'count'. 

	int res = sqlite3_exec(db, result.c_str(), getnum, &count, &zErrMsg);
	if (res != SQLITE_OK) 
	{
		std::string err;
		if (zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}
	return count;
}

bool DatabaseAccess::isUserTagged(int pictureId, int userId)
/*
	the function checks if a user is tagged in a picture, by counting the number of times a tag with the id of the picture and id of the user appears,
	using the query function 'count(*)' . 
*/
{
	//query will look like this: "select count(*) from tags where picture_id = 10 & user_id = 9";
	const char* GetTags = "select count(*) from tags where picture_id = ";
	std::string result = GetTags + std::to_string(pictureId) + " AND user_id = " + std::to_string(userId) + ";";

	char* zErrMsg = 0;
	int count = 0;
	// like in the function 'isUserTagged' from above, the call to the callback function will be to the function 'getnum' 
	//and the number will enter into 'count'. 
	int res = sqlite3_exec(db, result.c_str(), getnum, &count, &zErrMsg);
	if (res != SQLITE_OK)
	{
		std::string err;
		if (zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}

	//if count is 0, the user is not tagged in this picture.
	if (count == 0)
		return false;
	else
		return true;
}

void DatabaseAccess::printPic(const Picture& pic)
/*
	the function prints the info about the picture with the updated data.
*/
{
	std::list<int> usersTagged = getUsersTagged(pic.getId());

	std::cout << "Picture@" << pic.getId() << ": ["
		<< pic.getName() << ", " << pic.getCreationDate() << ", " << pic.getPath() <<
		"] " << getTagsCount(pic.getId()) << " users tagged : ";

	for (const auto user : usersTagged) {
		std::cout << "(" << getUser(user) << ") ";
	}
	std::cout << std::endl;

}


void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
/*
	function creates a new picture and add it to the picture in the server, with an insert query,
	using the picture object and the album name.
*/
{
	//query will look like : "insert into pictures(name, location, creation_date, album_id)  values ('my Trip', 'c://images', '12/2/2020', 5); "
	const char* insertPic = "insert into pictures(name, location, creation_date, album_id)  values ('";
	std::string result = insertPic + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', " + std::to_string(getId(albumName)) + ");";

	//sending the query to the 'sendToSQL' function that transfer it to the server and return true if it worked and false if not.
	// if it didn't work, alert it by throwing an exception.
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with adding the picture " + picture.getName() + " to album " + albumName + "\n");
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
/*
	function deletes from the server the picture by it's name.
*/
{
	//query will look like : "delete from pictures where name = 'Me'; "
	const char* deletePic = "delete from pictures where name = '";
	std::string result = deletePic + pictureName + "';";

	//sending the query to the 'sendToSQL' function that transfer it to the server and return true if it worked and false if not.
	// if it didn't work, alert it by throwing an exception.
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with deleting the picture " + pictureName + " from album " + albumName + "\n");
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
//the function tags a user in a picture by adding a new record in the tags table.
{
	//query will look like : " insert into tags(user_id, picture_id) values(3,2) ; "
	const char* addTag = "insert into tags(user_id, picture_id) values(";
	std::string result = addTag +std::to_string(userId)+ ", " + std::to_string(getPicture(pictureName).getId()) + ");";

	//sending the query to the 'sendToSQL' function that transfer it to the server and return true if it worked and false if not.
	// if it didn't work, alert it by throwing an exception.
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with tagging the user " + std::to_string(userId) + " in picture " + pictureName + " from album " + albumName + "\n");
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
//the function untags a user in a picture by deleting the record of it from the tags table.
{
	//query will look like : "delete from tags where user_id = 2 and picture_id = 4; "
	const char* addTag = "delete from tags where user_id = ";
	std::string result = addTag + std::to_string(userId) + " and picture_id = " + std::to_string(getPicture(pictureName).getId()) + ";";

	//sending the query to the 'sendToSQL' function that transfer it to the server and return true if it worked and false if not.
	// if it didn't work, alert it by throwing an exception.
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with untagging the user " + std::to_string(userId) + " in picture " + pictureName + " from album " + albumName + "\n");
}



//User Related:

 std::list<User> DatabaseAccess::getAllUsers()
//function return a list to all the users in the server.
{
	
	std::string result = "SELECT * FROM USERS ;";

	User* puser = new User();

	char* zErrMsg = 0;

	//Execute SQL statement
	//we are sending a pointer to User to the function, and we get it back from 'getAllUsersFromServer' with all the new data.
	int res = sqlite3_exec(db, result.c_str(), getAllUsersFromServer, puser, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}

	// since all the users are in the static list 'users', 
	// the variable 'allusers' gets the data from it, and 'users' is being cleared for it's next use.
	std::list<User> allusers = this->users;
	this->users.clear();
	return allusers;
}

void DatabaseAccess::printUsers()
//the function prints all the users in the server.
{
	const char* GetAlbums = "SELECT * FROM Users;";
	//the printing will be from the function 'callback', that the function 'sendToSQL' calls to, which this function calls to.
	if (sendToSQL(GetAlbums) == false) 
		throw MyException("something went wrong with printing the users.\n");
}

void DatabaseAccess::createUser(User& user)
//function creates a new user and add it to the users table in the server, with an insert query,
//using the User object.
{
	//query will look like : " insert into users(id, name) values ( 201, 'Eric' ) ; "
	const char* GetUsers = "insert into users (id, name) values (";
	std::string result = GetUsers + std::to_string(user.getId()) + ", '" + user.getName() + "');";

	//if the call to 'sendToSQL' fails, the query failed and the user wasn't inserted into the table.
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with creating the user " + user.getName() + "\n");
}

void DatabaseAccess::deleteUser(const User& user)
// the function removes the user from the Users table and cleans all of his data before.
{
	//cleaning user's data before deleting it
	cleanUserData(user);

	//query will look like this: "DELETE FROM USERS WHERE ID = 5 ;"
	const char* GetUsers = "DELETE FROM USERS WHERE ID =";
	std::string result = GetUsers + std::to_string(user.getId()) + ";";

	//if the call to 'sendToSQL' fails, the query failed and the user wasn't removed from the table.
	if (sendToSQL(result.c_str()) == false)
		throw MyException("something went wrong with deleting the user " + user.getName() + "\n");
}

bool DatabaseAccess::doesUserExists(int userId)
//the function check if there is a user with that id in the server,
// by selecting it and checking if its empty or not.
{
	//query will look like : "SELECT * FROM USERS WHERE USER_ID = 5;"
	const char* GetUsers = "SELECT * FROM Users WHERE ID =";
	std::string result = GetUsers + std::to_string(userId) + ";";

	char* zErrMsg = 0;
	User* puser = new User();
	//Execute SQL statement 
	int res = sqlite3_exec(db, result.c_str(), getUserFromServer, puser, &zErrMsg);

	if (res != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		throw "db exception";
	}

	// so after the call to 'getUserFromServer' we have all the info about the User in the 'puser' pointer
	// and i check the data in it, see if it's like it's suposed to be and not empty
	if (puser->getId() == userId)
		return true;
	else
		return false;
}

User DatabaseAccess::getUser(int userId)
//the function returns a User, according to the id.
{
	//query will look like : "SELECT * FROM USERS WHERE USER_ID = 5;"
	const char* GetUsers = "SELECT * FROM Users WHERE ID =";
	std::string result = GetUsers + std::to_string(userId) + ";";

	User* puser = new User();

	char* zErrMsg = 0;
	//running the sql query. the user data will be in the variable 'puser', after the call to 'getUserFromServer'.
	int res = sqlite3_exec(db, result.c_str(), getUserFromServer, puser, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err;
		if(zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw MyException("SQL error: " + err + "\n");
	}

	return *puser;
}

int DatabaseAccess::lastId()
//calling to the last user created (or with the gratest id), and return it, to find the last id that is in use.
{
	const char* Getid = "SELECT * FROM Users order by ID desc limit 1;";
	User* puser = new User();

	char* zErrMsg = 0;
	/* Execute SQL statement
	   we are sending an empty User to the function, and we get it back from 'getUserFromServer' with all the new data
	*/
	int res = sqlite3_exec(db, Getid, getUserFromServer, puser, &zErrMsg);

	if (res != SQLITE_OK) {
		std::string err;
		if (zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}
	//the last id:
	int id = puser->getId();

	return id;
}


//User Statistics:

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
//function checks how many albums the user has in the server.
{
	//the function 'getAlbumsOfUser' returns a list with all of the albums of the user, and the return value will be the size of it.
	std::list<Album> albums = getAlbumsOfUser(user);
	return albums.size();
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
//function returns the amount of albums in which the user is tagged.
{
	std::list<Album> albumsOfUser = getAlbums();
	std::list<Album>::iterator Albumsit;
	int albumsCount = 0;
	//running on the list of all the albums in the server
	for (Albumsit = albumsOfUser.begin(); Albumsit != albumsOfUser.end(); ++Albumsit)
	{
		//getting list of all the pictures in this album (Albumsit), and setting an iterator for it.
		std::list<Picture> pics = getPictures(Albumsit->getName());
		std::list<Picture>::iterator Picit;
		//running on the list of the pictures and checking if the user is tagged in it.
		//if so, breaking the loop after increasing the number of albums the user is tagged in - 'albumsCount'.
		for (Picit = pics.begin(); Picit != pics.end(); ++Picit)
		{
			if (isUserTagged(Picit->getId(), user.getId()) == true) 
			{
				albumsCount++;
				break;
			}
		}
	}
	return albumsCount;
}

int DatabaseAccess::countTagsOfUser(const User& user)
//function returns the number of times the user is tagged in all the pictures in the server.
{
	//query will look like : "select count(*) from tags where user_id = 5 ;"
	const char* GetTags = "select count(*) from tags where user_id = ";
	std::string result = GetTags + std::to_string(user.getId()) + ";";

	char* zErrMsg = 0;
	int count = 0;
	int res = sqlite3_exec(db, result.c_str(), getnum, &count, &zErrMsg);
	if (res != SQLITE_OK) {
		std::string err;
		if (zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}
	return count;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
//returns average tags of the user per album,
//which means (the number of tags the user have / number of albums he is tagged in).
{
	float numTags = countTagsOfUser(user);
	float numAlbumsTagged = countAlbumsTaggedOfUser(user);
	float result;
	if (numAlbumsTagged != 0)
		result = numTags/ numAlbumsTagged;
	else
		result = 0;
	return result;
}

std::list<int> DatabaseAccess::getUsersTagged(const int& pictureId)
//the function returns a list of the user_id of the users that are tagged in a picture. 
{
	//query  will look like: " select user_id from tags where picture_id = 5; "
	const char* GetTags = "select user_id from tags where picture_id = ";
	std::string result = GetTags + std::to_string(pictureId) + ";";

	char* zErrMsg = 0;
	//this time the list is sent directly to the function 'getUsersId', and this is the variable to return.

	std::list<int> usresId;
	int res = sqlite3_exec(db, result.c_str(), getUsersId, &usresId, &zErrMsg);
	if (res != SQLITE_OK)
	{
		std::string err;
		if (zErrMsg != NULL)
			err = zErrMsg;
		sqlite3_free(zErrMsg);
		throw ("SQL error: " + err + "\n");
	}
	return usresId;
}


