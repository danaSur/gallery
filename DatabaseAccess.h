#pragma once
#include <list>
#include "Album.h"
#include "User.h"
#include "Picture.h"
#include "IDataAccess.h"
#include "sqlite3.h"
#include <io.h>
#include <vector>
#include "MyException.h"

class DatabaseAccess : public IDataAccess
{
private:
	sqlite3* db;
	std::string db_name = "GalleryDB.sqlite";
	static std::list<Album> albums;
	static std::list<Picture> pictures;
	static std::list<User> users;

	//during the connection with the SQL server, the data is being sent back through one of the "callback" functions,
	//each one is adjusted to it's purposes, like returning a list of Pictures, returning a single User or a single number.

	//simple callback function, that prints info or used to send simple queries, such as insert or delete queries.
	static int callback(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		int i;
		for (i = 0; i < argc; i++)
		{
			std::cout << argv[i] << "\t";
		}
		printf("\n");
		return 0;
	}

	//callback function that cast the void* parameter into a single object, User / Album / Picture.
	static int getUserFromServer(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		User* u = (User*)(NotUsed);
		int i;
		for (i = 0; i < argc; i++) {
			if (i == 0)
				u->setId(atoi(argv[i]));
			if (i == 1)
				u->setName(argv[i]);
		}
		//printf("\n");
		return 0;
	}
	static int getAlbumsFromServer(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		Album* a = (Album*)(NotUsed);
		int i;
		for (i = 0; i < argc; i++)
		{
			if (i == 0)
			{
				// ID parameter
			}
			if (i == 1)
			{
				a->setName(argv[i]);
			}
			if (i == 2)
			{
				a->setOwner(atoi(argv[i]));
			}
			if (i == 3)
			{
				a->setCreationDate(argv[i]);
			}
		}

		return 0;
	}
	static int getPictureFromServer(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		Picture* p = (Picture*)(NotUsed);
		int i;
		for (i = 0; i < argc; i++)
		{
			if (i == 0)
			{
				p->setId(atoi(argv[i]));
			}
			if (i == 1)
			{
				p->setName(argv[i]);
			}
			if (i == 2)
			{
				p->setPath(argv[i]);
			}
			if (i == 3)
			{
				p->setCreationDate(argv[i]);
			}
		}

		return 0;
	}

	//callback function that cast the void* parameter into a single object, User / Album / Picture, but enter it into a static list, according to the type of the object.
	static int getAllAlbumsFromServer(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		Album* a = (Album*)(NotUsed);
		int i;
		for (i = 0; i < argc; i++)
		{
			int id, userid;
			std::string name, loc;
			if (i == 0)
			{
				// ID parameter
			}
			if (i == 1)
			{
				a->setName(argv[i]);
			}
			if (i == 2)
			{
				a->setOwner(atoi(argv[i]));
			}
			if (i == 3)
			{
				a->setCreationDate(argv[i]);
			}

		}
		albums.push_back(*a);
		return 0;
	}
	static int getAllPicturesFromServer(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		Picture* p = (Picture*)(NotUsed);
		int i;
		for (i = 0; i < argc; i++)
		{
			if (i == 0)
			{
				p->setId(atoi(argv[i]));
			}
			if (i == 1)
			{
				p->setName(argv[i]);
			}
			if (i == 2)
			{
				p->setPath(argv[i]);
			}
			if (i == 3)
			{
				p->setCreationDate(argv[i]);
			}
		}
		pictures.push_back(*p);
		return 0;
	}
	static int getAllUsersFromServer(void* NotUsed, int argc, char** argv, char** azColName)
	{
		//argc : number of colums
		// argv: the data of the record.
		//for the work with sqlite
		User* u = (User*)(NotUsed);
		int i;
		for (i = 0; i < argc; i++) {
			if (i == 0)
				u->setId(atoi(argv[i]));
			if (i == 1)
				u->setName(argv[i]);
		}
		users.push_back(*u);
		return 0;
	}


	//callback function that cast the void* parameter into an integer, single one or a list.
	static int getnum(void* NotUsed, int argc, char** argv, char** azColName)
	{
		if (argc == 1 && argv) {
			*static_cast<int*>(NotUsed) = atoi(argv[0]);
			return 0;
		}
		return 1;
	}
	static int getUsersId(void* NotUsed, int argc, char** argv, char** azColName)
	{
		if (argc == 1 && argv) {
			
			static_cast<std::list<int>*>(NotUsed)->push_back(atoi(argv[0]));
			return 0;
		}
		return 1;
	}

	//the function that sends a query to the server, with the simple callback function (callback), and return true if the quary worked, false if not.
	bool sendToSQL(const char* query)
	{
		char* zErrMsg = 0;
		/* Execute SQL statement */
		int res = sqlite3_exec(db, query, callback, 0, &zErrMsg);

		if (res != SQLITE_OK) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
			return false;
		}
		else
		{
			return true;
		}
	}

	void cleanUserData(const User & userId);


public:
	DatabaseAccess() = default;
	virtual ~DatabaseAccess() = default;

	 //album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override ;
	void createAlbum(const Album& album) override ;
	void deleteAlbum(const std::string& albumName, int userId) override ;
	bool doesAlbumExists(const std::string& albumName, int userId) override ;
	Album openAlbum(const std::string& albumName) override ;
	void closeAlbum(Album& pAlbum) override {}; 
	void printAlbums() override ;
	// function that returns the id in the DB of a album by name
	int getId(const std::string& albumName);

	 //picture related
	//functions that are in the class 'Album' and need to be re-write:
	bool doesPictureExists(const std::string& name);
	std::list<Picture> getPictures(const std::string& albumName);
	std::list<Picture> getAllPictures();
	Picture getPicture(const std::string& pictureName);
	int getTagsCount(const int& pictureId);
	bool isUserTagged(int pictureId, int userId);
	void printPic(const Picture& pic);

	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override ;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override ;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override ;

	// user related
	std::list<User> getAllUsers();
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override ;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;
	// function that returns the last id in the DB
	int lastId();

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override ;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override ;
	std::list<int> getUsersTagged(const int& pictureId);

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	bool open() override;
	void close() override
	{
		sqlite3_close(db);
		db = nullptr;
	};
	void clear() override;

};
