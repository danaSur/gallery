#include "AlbumManager.h"
#include <iostream>
#include "Constants.h"
#include "AlbumNotOpenException.h"


AlbumManager::AlbumManager(DatabaseAccess& dataBaseAccess) 
	: db(dataBaseAccess), m_nextPictureId(100), m_nextUserId(200)
{
	db.open();
	m_nextUserId = db.lastId();
}

void AlbumManager::executeCommand(CommandType command) {
	try {
		AlbumManager::handler_func_t handler = m_commands.at(command);
		(this->*handler)();
	}
	catch (const std::out_of_range&) {
		throw MyException("Error: Invalid command[" + std::to_string(command) + "]\n");
	}
}

void AlbumManager::printHelp() const
{
	std::cout << "Supported Album commands:" << std::endl;
	std::cout << "*************************" << std::endl;

	for (const struct CommandGroup& group : m_prompts) {
		std::cout << group.title << std::endl;
		std::string space(".  ");
		for (const struct CommandPrompt& command : group.commands) {
			space = command.type < 10 ? ".   " : ".  ";

			std::cout << command.type << space << command.prompt << std::endl;
		}
		std::cout << std::endl;
	}
}


// ******************* Album ******************* 
void AlbumManager::createAlbum()
{
	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId)) 
		throw MyException("Error: Can't create album since there is no user with id [" + userIdStr + "]\n");
	

	std::string name = getInputFromConsole("Enter album name - ");
	if (db.doesAlbumExists(name, userId)) 
		throw MyException("Error: Failed to create album, album with the same name already exists\n");
	

	Album newAlbum(userId, name);
	db.createAlbum(newAlbum);

	std::cout << "Album [" << newAlbum.getName() << "] created successfully by user@" << newAlbum.getOwnerId() << std::endl;
}

void AlbumManager::openAlbum()
{
	if (isCurrentAlbumSet()) {
		closeAlbum();
	}

	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId)) {
		throw MyException("Error: Can't open album since there is no user with id @" + userIdStr + ".\n");
	}
	std::string name = getInputFromConsole("Enter album name - ");
	if (!db.doesAlbumExists(name, userId)) {
		throw MyException("Error: Failed to open album, since there is no album with name:" + name + ".\n");
	}
	m_openAlbum = db.openAlbum(name);
	m_currentAlbumName = name;
	// success
	std::cout << "Album [" << name << "] opened successfully." << std::endl;
}

void AlbumManager::closeAlbum()
{
	refreshOpenAlbum();

	std::cout << "Album [" << m_openAlbum.getName() << "] closed successfully." << std::endl;
	db.closeAlbum(m_openAlbum);
	m_currentAlbumName = "";
}

void AlbumManager::deleteAlbum()
{
	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId))
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	

	std::string albumName = getInputFromConsole("Enter album name - ");
	if (!db.doesAlbumExists(albumName, userId)) 
		throw MyException("Error: Failed to delete album, since there is no album with name:" + albumName + ".\n");
	

	if ((isCurrentAlbumSet()) &&
		(m_openAlbum.getOwnerId() == userId && m_openAlbum.getName() == albumName)) {

		closeAlbum();
	}

	db.deleteAlbum(albumName, userId);
	std::cout << "Album [" << albumName << "] @" << userId << " deleted successfully." << std::endl;
}

void AlbumManager::listAlbums()
{
	db.printAlbums();
}

void AlbumManager::listAlbumsOfUser()
{
	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId))
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");

	const User& user = db.getUser(userId);
	const std::list<Album>& albums = db.getAlbumsOfUser(user);

	std::cout << "Albums list of user@" << user.getId() << ":" << std::endl;
	std::cout << "-----------------------" << std::endl;

	for (const auto& album : albums)
	{
		std::cout << "   + [" << album.getName() << "] - created on " << album.getCreationDate() << std::endl;
	}
}


// ******************* Picture ******************* 
void AlbumManager::addPictureToAlbum()
{
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	//if (m_openAlbum.doesPictureExists(picName)) 
	if (db.doesPictureExists(picName)) 
		throw MyException("Error: Failed to add picture, picture with the same name already exists.\n");
	

	Picture picture(++m_nextPictureId, picName);
	std::string picPath = getInputFromConsole("Enter picture path: ");
	picture.setPath(picPath);

	//m_dataAccess.addPictureToAlbumByName(m_openAlbum.getName(), picture);
	db.addPictureToAlbumByName(m_openAlbum.getName(), picture);

	std::cout << "Picture [" << picture.getId() << "] successfully added to Album [" << m_openAlbum.getName() << "]." << std::endl;
}

void AlbumManager::removePictureFromAlbum()
{
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	if (!db.doesPictureExists(picName)) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	
	auto picture = db.getPicture(picName);

	//delete all tags in it:
	std::list<User> users = db.getAllUsers();
	std::list<User>::iterator it;
	for (it = users.begin(); it != users.end(); ++it)
	{
		if (db.isUserTagged(picture.getId(), it->getId()))
			db.untagUserInPicture(m_openAlbum.getName(), picName, it->getId());
	}

	db.removePictureFromAlbumByName(m_openAlbum.getName(), picture.getName());
	std::cout << "Picture <" << picName << "> successfully removed from Album [" << m_openAlbum.getName() << "]." << std::endl;
}

void AlbumManager::listPicturesInAlbum()
{
	refreshOpenAlbum();

	std::cout << "List of pictures in Album [" << m_openAlbum.getName()
		<< "] of user@" << m_openAlbum.getOwnerId() << ":" << std::endl;

	const std::list<Picture>& albumPictures = db.getPictures(m_openAlbum.getName());
	for (auto iter = albumPictures.begin(); iter != albumPictures.end(); ++iter) {
		std::cout << "   + Picture [" << iter->getId() << "] - " << iter->getName() <<
			"\tLocation: [" << iter->getPath() << "]\tCreation Date: [" <<
			iter->getCreationDate() << "]\tTags: [" << db.getTagsCount(iter->getId()) << "]" << std::endl;
	}
	std::cout << std::endl;
}

void AlbumManager::showPicture()
{

	
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	if (!db.doesPictureExists(picName)) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	
	auto pic = db.getPicture(picName);
	if (!fileExistsOnDisk(pic.getPath())) 
		throw MyException("Error: Can't open <" + picName + "> since it doesnt exist on disk.\n");
	
	std::string program = getInputFromConsole("Enter program to open the picture with (Painter or Photos): ");
	std::string programToOpenWith;
	if(program == "Painter" || program == "painter")
		programToOpenWith = "mspaint.exe ";
	else if(program == "Photos" || program == "photos")
		programToOpenWith = "rundll32 \"C:\\Program Files\\Windows Photo Viewer\\PhotoViewer.dll\" ImageView_Fullscreen ";
	else
		throw MyException("Error: the Gallery doesn't support this program.\n");

	STARTUPINFOW process_startup_info{ 0 };
	process_startup_info.cb = sizeof(process_startup_info); // setup size of strcture in bytes

	PROCESS_INFORMATION process_info{ 0 };

	std::string processName = programToOpenWith + pic.getPath();
	LPWSTR commandline_args = ConvertString(processName);


	if (CreateProcessW(NULL, commandline_args, NULL, NULL, TRUE, 0, NULL, NULL, &process_startup_info, &process_info))
	{
		WaitForSingleObject(process_info.hProcess, INFINITE); 
		CloseHandle(process_info.hProcess);
		CloseHandle(process_info.hThread);
	}
}

void AlbumManager::tagUserInPicture()
{
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	if (!db.doesPictureExists(picName)) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");

	Picture pic = db.getPicture(picName);

	std::string userIdStr = getInputFromConsole("Enter user id to tag: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	
	User user = db.getUser(userId);

	if (db.isUserTagged(pic.getId(), user.getId()))
		throw MyException("Error: The user is already tagged! \n");

	db.tagUserInPicture(m_openAlbum.getName(), pic.getName(), user.getId());
	std::cout << "User @" << userIdStr << " successfully tagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;
}

void AlbumManager::untagUserInPicture()
{
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	if (!db.doesPictureExists(picName)) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	

	Picture pic = db.getPicture(picName);

	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = stoi(userIdStr);
	if (!db.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	
	User user = db.getUser(userId);
 
	if (!db.isUserTagged(pic.getId(), user.getId())) 
		throw MyException("Error: The user was not tagged! \n");
	

	db.untagUserInPicture(m_openAlbum.getName(), pic.getName(), user.getId());
	std::cout << "User @" << userIdStr << " successfully untagged in picture <" << pic.getName() << "> in album [" << m_openAlbum.getName() << "]" << std::endl;

}

void AlbumManager::listUserTags()
{
	refreshOpenAlbum();

	std::string picName = getInputFromConsole("Enter picture name: ");
	//if (!m_openAlbum.doesPictureExists(picName)) 
	if (!db.doesPictureExists(picName)) 
		throw MyException("Error: There is no picture with name <" + picName + ">.\n");
	
	//auto pic = m_openAlbum.getPicture(picName);
	auto pic = db.getPicture(picName);

	//const std::set<int> users = pic.getUserTags();
	std::list<int> usersId = db.getUsersTagged(pic.getId());

	if (0 == usersId.size()) {
		throw MyException("Error: There is no user tagged in <" + picName + ">.\n");
	}

	std::cout << "Tagged users in picture <" << picName << ">:" << std::endl;
	for (const int user_id : usersId) {
		//const User user = m_dataAccess.getUser(user_id);
		const User user = db.getUser(user_id);
		std::cout << user << std::endl;
	}
	std::cout << std::endl;

}


// ******************* User ******************* 
void AlbumManager::addUser()
{
	std::string name = getInputFromConsole("Enter user name: ");

	User user(++m_nextUserId, name);

	db.createUser(user);
	std::cout << "User " << name << " with id @" << user.getId() << " created successfully." << std::endl;
}

void AlbumManager::removeUser()
{
	// get user name
	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	
	const User& user = db.getUser(userId);
	if (isCurrentAlbumSet() && userId == m_openAlbum.getOwnerId()) 
		closeAlbum();
	
	db.deleteUser(user);
	std::cout << "User @" << userId << " deleted successfully." << std::endl;
}

void AlbumManager::listUsers()
{
	db.printUsers();
}

void AlbumManager::userStatistics()
{
	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	if (!db.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");

	const User& user = db.getUser(userId);

	std::cout << "user @" << userId << " Statistics:" << std::endl << "--------------------" << std::endl <<
		"  + Count of Albums Tagged: " << db.countAlbumsTaggedOfUser(user) << std::endl <<
		"  + Count of Tags: " << db.countTagsOfUser(user) << std::endl <<
		"  + Avarage Tags per Album: " << db.averageTagsPerAlbumOfUser(user) << std::endl <<
		"  + Number Of Albums: " << db.countAlbumsOwnedOfUser(user) << std::endl;
}


// ******************* Queries ******************* 
void AlbumManager::topTaggedUser()
{
	const User& user = db.getTopTaggedUser();

	std::cout << "The top tagged user is: " << user.getName() << std::endl;
}

void AlbumManager::topTaggedPicture()
{
	//const Picture& picture = m_dataAccess.getTopTaggedPicture();
	const Picture& picture = db.getTopTaggedPicture();

	std::cout << "The top tagged picture is: " << picture.getName() << std::endl;
}

void AlbumManager::picturesTaggedUser()
{
	std::string userIdStr = getInputFromConsole("Enter user id: ");
	int userId = std::stoi(userIdStr);
	//if (!m_dataAccess.doesUserExists(userId)) 
	if (!db.doesUserExists(userId)) 
		throw MyException("Error: There is no user with id @" + userIdStr + "\n");
	

	//auto user = m_dataAccess.getUser(userId);
	auto user = db.getUser(userId);

	//auto taggedPictures = m_dataAccess.getTaggedPicturesOfUser(user);
	auto taggedPictures = db.getTaggedPicturesOfUser(user);

	std::cout << "List of pictures that User@" << user.getId() << " tagged :" << std::endl;
	for (const Picture& picture : taggedPictures) {
		std::cout << "   + ";
		db.printPic(picture);
		std::cout << std::endl;
	}
	std::cout << std::endl;
}


// ******************* Help & exit ******************* 
void AlbumManager::exit()
{
	std::exit(EXIT_SUCCESS);
}

void AlbumManager::help()
{
	system("CLS");
	printHelp();
}

std::string AlbumManager::getInputFromConsole(const std::string& message)
{
	std::string input;
	do 
	{
		std::cout << message;
		std::getline(std::cin, input);
	} while (input.empty());

	return input;
}

bool AlbumManager::fileExistsOnDisk(const std::string& filename)
{
	struct stat buffer;
	return (stat(filename.c_str(), &buffer) == 0);
}

void AlbumManager::refreshOpenAlbum() {
	if (!isCurrentAlbumSet()) {
		throw AlbumNotOpenException();
	}
	//m_openAlbum = m_dataAccess.openAlbum(m_currentAlbumName);
	m_openAlbum = db.openAlbum(m_currentAlbumName);
}

bool AlbumManager::isCurrentAlbumSet() const
{
	return !m_currentAlbumName.empty();
}

LPWSTR AlbumManager::ConvertString(std::string& instr)
{
	{
		// Assumes std::string is encoded in the current Windows ANSI codepage
		int bufferlen = ::MultiByteToWideChar(CP_ACP, 0, instr.c_str(), instr.size(), NULL, 0);
		if (bufferlen == 0)
		{
			// Something went wrong. Perhaps, check GetLastError() and log.
			return 0;
		}
		// Allocate new LPWSTR - must deallocate it later
		LPWSTR widestr = new WCHAR[bufferlen + 1];

		::MultiByteToWideChar(CP_ACP, 0, instr.c_str(), instr.size(), widestr, bufferlen);

		// Ensure wide string is null terminated
		widestr[bufferlen] = 0;

		// Do something with widestr
		return widestr;
	}
}



const std::vector<struct CommandGroup> AlbumManager::m_prompts = {
	{
		"Supported Albums Operations:\n----------------------------",
		{
			{ CREATE_ALBUM        , "Create album" },
			{ OPEN_ALBUM          , "Open album" },
			{ CLOSE_ALBUM         , "Close album" },
			{ DELETE_ALBUM        , "Delete album" },
			{ LIST_ALBUMS         , "List albums" },
			{ LIST_ALBUMS_OF_USER , "List albums of user" }
		}
	},
	{
		"Supported Album commands (when specific album is open):",
		{
			{ ADD_PICTURE    , "Add picture." },
			{ REMOVE_PICTURE , "Remove picture." },
			{ SHOW_PICTURE   , "Show picture." },
			{ LIST_PICTURES  , "List pictures." },
			{ TAG_USER		 , "Tag user." },
			{ UNTAG_USER	 , "Untag user." },
			{ LIST_TAGS		 , "List tags." }
		}
	},
	{
		"Supported Users commands: ",
		{
			{ ADD_USER         , "Add user." },
			{ REMOVE_USER      , "Remove user." },
			{ LIST_OF_USER     , "List of users." },
			{ USER_STATISTICS  , "User statistics." },
		}
	},
	{
		"Supported Queries:",
		{
			{ TOP_TAGGED_USER      , "Top tagged user." },
			{ TOP_TAGGED_PICTURE   , "Top tagged picture." },
			{ PICTURES_TAGGED_USER , "Pictures tagged user." },
		}
	},
	{
		"Supported Operations:",
		{
			{ HELP , "Help (clean screen)" },
			{ EXIT , "Exit." },
		}
	}
};

const std::map<CommandType, AlbumManager::handler_func_t> AlbumManager::m_commands = {
	{ CREATE_ALBUM, &AlbumManager::createAlbum },
	{ OPEN_ALBUM, &AlbumManager::openAlbum },
	{ CLOSE_ALBUM, &AlbumManager::closeAlbum },
	{ DELETE_ALBUM, &AlbumManager::deleteAlbum },
	{ LIST_ALBUMS, &AlbumManager::listAlbums },
	{ LIST_ALBUMS_OF_USER, &AlbumManager::listAlbumsOfUser },
	{ ADD_PICTURE, &AlbumManager::addPictureToAlbum },
	{ REMOVE_PICTURE, &AlbumManager::removePictureFromAlbum },
	{ LIST_PICTURES, &AlbumManager::listPicturesInAlbum },
	{ SHOW_PICTURE, &AlbumManager::showPicture },
	{ TAG_USER, &AlbumManager::tagUserInPicture, },
	{ UNTAG_USER, &AlbumManager::untagUserInPicture },
	{ LIST_TAGS, &AlbumManager::listUserTags },
	{ ADD_USER, &AlbumManager::addUser },
	{ REMOVE_USER, &AlbumManager::removeUser },
	{ LIST_OF_USER, &AlbumManager::listUsers },
	{ USER_STATISTICS, &AlbumManager::userStatistics },
	{ TOP_TAGGED_USER, &AlbumManager::topTaggedUser },
	{ TOP_TAGGED_PICTURE, &AlbumManager::topTaggedPicture },
	{ PICTURES_TAGGED_USER, &AlbumManager::picturesTaggedUser },
	{ HELP, &AlbumManager::help },
	{ EXIT, &AlbumManager::exit }
};
